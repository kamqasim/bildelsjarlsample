//
//  Font.swift
//  RiksKampen
//
//  Created by Ikotel on 12/4/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//

import Foundation
import UIKit

class FontName{

    static var montserratRegular    : String = "Montserrat-Regular"
    static var montserratMedium     : String = "Montserrat-Medium"
    static var montserratSemiBold   : String = "Montserrat-SemiBold"
    static var montserratBold       : String = "Montserrat-Bold"

    static var vVerySmallFontSize : CGFloat  = 8.0
    static var verysmallFontSize : CGFloat  = 10.0
    static var smallFontSize : CGFloat  = 13.0
    static var normalFontSize : CGFloat  = 15.0
    static var largeFontSize : CGFloat  = 20.0
    
}
