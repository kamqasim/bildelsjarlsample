//
//  AppHelper.swift
//  BildelBasenClean
//
//  Created by Apple on 10/16/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation
import UIKit
import LocalAuthentication

class AppHelper {

}


/// Get device related information
extension AppHelper {

    static func getDeviceID() -> String {

        return UIDevice.current.identifierForVendor!.uuidString
    }

    static func getBuildNumber() -> String {

        return Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
    }

    static func getVersionNumber() -> String {

        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
    }

    static func getCurrentLanguage() -> String {

        return Locale.current.languageCode!
    }

    class var hasTopNotch: Bool {
        if #available(iOS 11.0, tvOS 11.0, *) {
            // 44.0 on iPhone X, 20.0 on iPhone 8 on iOS 12+.
            return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
        }
        return false
    }

}

struct Defaults {

    static let (userIdKey, authTokenKey) = ("userId", "token")
    static let userSessionKey = "com.save.usersession"
    private static let userDefault = UserDefaults.standard

    /**
       - Description - It's using for the passing and fetching
                    user values from the UserDefaults.
     */
    struct UserDetails {
        let userId: String
        let authToken: String

        init(_ json: [String: String]) {
            self.userId = json[userIdKey] ?? ""
            self.authToken = json[authTokenKey] ?? ""
        }
    }

    /**
     - Description - Saving user details
     - Inputs - name `String` & address `String`
     */
    static func save(_ userID: String, authToken: String){
        userDefault.set([userIdKey: userID, authTokenKey: authToken],
                        forKey: userSessionKey)
    }

    /**
     - Description - Fetching Values via Model `UserDetails` you can use it based on your uses.
     - Output - `UserDetails` model
     */
    static func getUserIdAndAuthToken()-> UserDetails {
        return UserDetails((userDefault.value(forKey: userSessionKey) as? [String: String]) ?? [:])
    }

    /**
        - Description - Clearing user details for the user key `com.save.usersession`
     */
    static func clearUserData(){
        userDefault.removeObject(forKey: userSessionKey)
    }
}
