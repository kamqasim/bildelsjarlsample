//
//  AppLocalNetworkRoute.swift
//  BildelsJarl
//
//  Created by Apple on 11/4/19.
//  Copyright © 2019 Apple. All rights reserved.
//

//
import Foundation
import UIKit



enum Storyboard: String {

    case launchScreenStory = "LaunchScreen"
    case loginStoryboard = "LoginStory"
    case signUpStoryBoard = "SignUpStory"

}

class AppLocalNetworkRoute: NSObject {

    private static let launchScreenStoryboard = UIStoryboard(name: Storyboard.launchScreenStory.rawValue, bundle: nil)
    private static let loginStoryboard = UIStoryboard(name: Storyboard.loginStoryboard.rawValue, bundle: nil)
    private static let signUpStoryBoard = UIStoryboard(name: Storyboard.signUpStoryBoard.rawValue, bundle: nil)


    static func getLoginVC() -> UIViewController {
        return loginStoryboard.instantiateViewController(withIdentifier: "KQLoginViewController") as! KQLoginViewController
    }

    static func getSignUpView() -> UIViewController {
        return signUpStoryBoard.instantiateViewController(withIdentifier: "KQSignUpViewController") as! KQSignUpViewController
    }

}




//
//  AppLocalNetworkRoute.swift
//  BildelsJarl
//
//  Created by Apple on 11/4/19.
//  Copyright © 2019 Apple. All rights reserved.
//

//
import Foundation
import UIKit



enum Storyboards: String {

    case launchScreenStory = "LaunchScreen"
    case loginStoryboard = "LoginStory"
    case signUpStoryBoard = "SignUpStory"

    var storyboard:UIStoryboard{
        return UIStoryboard.init(name: self.rawValue, bundle: nil)
    }
    static func instatiate<T>(view:T.Type)->T?{
        let view = "\(view)"
        let storyboard = UIStoryboard.init(name: view, bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: view) as? T
    }
}

class AppLocalNetworkRoutes: NSObject {


    private static let launchScreenStoryboard = UIStoryboard(name: Storyboard.launchScreenStory.rawValue, bundle: nil)
    private static let loginStoryboard = UIStoryboard(name: Storyboard.loginStoryboard.rawValue, bundle: nil)
    private static let signUpStoryBoard = UIStoryboard(name: Storyboard.signUpStoryBoard.rawValue, bundle: nil)


    static func getLoginVC() -> UIViewController {
        return loginStoryboard.instantiateViewController(withIdentifier: "KQLoginViewController") as! KQLoginViewController

        

    }

    static func getSignUpView() -> UIViewController {
        return signUpStoryBoard.instantiateViewController(withIdentifier: "KQSignUpViewController") as! KQSignUpViewController
    }

}



