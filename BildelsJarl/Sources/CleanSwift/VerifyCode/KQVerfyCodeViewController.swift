//
//  KQVerfyCodeViewController.swift
//  BildelsJarl
//
//  Created by Apple on 11/5/19.
//  Copyright (c) 2019 Apple. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit
import SkyFloatingLabelTextField
protocol KQVerfyCodeDisplayLogic: class
{
    func displaySomething(viewModel: KQVerfyCode.Something.ViewModel)
    func displayVerfyCode()
}

class KQVerfyCodeViewController: UIViewController, KQVerfyCodeDisplayLogic
{



    //MARK:- IBoutlets
    @IBOutlet weak var txtDigitOne: KQVerifyCodeTextField!
    @IBOutlet weak var txtDigitTwo: KQVerifyCodeTextField!
    @IBOutlet weak var txtDigitThree: KQVerifyCodeTextField!
    @IBOutlet weak var txtDigitFour: KQVerifyCodeTextField!
    @IBOutlet weak var txtDigitFive: KQVerifyCodeTextField!
    //MARK:- Instance Variables

    var interactor: KQVerfyCodeBusinessLogic?
    var router: (NSObjectProtocol & KQVerfyCodeRoutingLogic & KQVerfyCodeDataPassing)?

    // MARK: Object lifecycle

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
    {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }

    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setup()
    }

    // MARK: Setup

    private func setup()
    {
        let viewController = self
        let interactor = KQVerfyCodeInteractor()
        let presenter = KQVerfyCodePresenter()
        let router = KQVerfyCodeRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }

    // MARK: Routing

    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let scene = segue.identifier {
            let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
            if let router = router, router.responds(to: selector) {
                router.perform(selector, with: segue)
            }
        }
    }

    override func viewDidLoad()
    {
        super.viewDidLoad()

        txtDigitOne.delegate    = self
        txtDigitTwo.delegate    = self
        txtDigitThree.delegate  = self
        txtDigitFour.delegate   = self
        txtDigitFive.delegate   = self

        txtDigitOne.becomeFirstResponder()
        handledDeletedDigitEvent()
        txtDigitOne.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        txtDigitTwo.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        txtDigitThree.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        txtDigitFour.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        txtDigitFive.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
    }

    deinit {
        print("VerifyCodeViewController dealloacted")
    }

    private func handledDeletedDigitEvent(){
        txtDigitTwo.hasDeleted = { [weak self]textField in
            self?.txtDigitOne.text = nil
            self?.txtDigitOne.becomeFirstResponder()
        }

        txtDigitThree.hasDeleted = {[weak self] _ in
            self?.txtDigitTwo.text = nil
            self?.txtDigitTwo.becomeFirstResponder()
        }
        txtDigitFour.hasDeleted = {[weak self] _ in
            self?.txtDigitThree.text = nil
            self?.txtDigitThree.becomeFirstResponder()
        }
    }
    private func emptyInputFields(){
        txtDigitOne.text = nil
        txtDigitTwo.text = nil
        txtDigitThree.text = nil
        txtDigitFour.text = nil
        txtDigitFive.text = nil
    }
    //MARK:- Action Methods
    @IBAction func btnResendCode(_ sender: Any) {

    }
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }


    // MARK: display Verfy Code

    // MARK:  Skip Action

    @IBAction func verifyCodeAction(_ sender: Any) {
        interactor?.verifyCode()
    }

    func displayVerfyCode() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            // your code here
            if let rout = self.router{
                print("Move to Sign Up")

            }
        }
    }


    func doSomething()
    {
        let request = KQVerfyCode.Something.Request()
        interactor?.doSomething(request: request)
    }

    func displaySomething(viewModel: KQVerfyCode.Something.ViewModel)
    {
        //nameTextField.text = viewModel.name
    }
}

extension KQVerfyCodeViewController: UITextFieldDelegate {

@objc func textFieldDidChange(textField: UITextField){

    let  text = textField.text
    var code = ""
    if text?.utf16.count == 1{
        switch textField {
        case txtDigitOne:txtDigitTwo.becomeFirstResponder()
        case txtDigitTwo:txtDigitThree.becomeFirstResponder()
        case txtDigitThree:txtDigitFour.becomeFirstResponder()
        case txtDigitFour:txtDigitFive.becomeFirstResponder()
        case txtDigitFive:txtDigitOne.becomeFirstResponder()

        code = txtDigitOne.text! + txtDigitTwo.text! + txtDigitThree.text! + txtDigitFour.text! + txtDigitFive.text!
            //print(code)
        default:
            break
        }
    }
  }
}
