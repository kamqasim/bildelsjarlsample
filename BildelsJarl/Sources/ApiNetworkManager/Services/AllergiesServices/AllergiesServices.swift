//
//  AllergiesBussiness.swift
//  BildelsJarl
//
//  Created by Apple on 10/30/19.
//  Copyright (c) 2019 Apple. All rights reserved.
//

import Foundation

import Foundation

class AllergiesServices {


    // MARK: - UserAllergies

    func allergiesList(parameters: [String: Any], completion: @escaping (AllergiesModelList?, Error?) -> ()) {
        // api
        let api = AllergiesApiHandler()
        // api loader
        let apiTaskLoader = APILoader(apiRequest: api)
        apiTaskLoader.loadAPIRequest(requestData: parameters) { (result) in
            switch result{
            case  .success(let allergiesList):print("allergiesList")
            completion(allergiesList,nil)
            case .failure(let error):
                completion(nil,error)
            }
        }
    }
}
