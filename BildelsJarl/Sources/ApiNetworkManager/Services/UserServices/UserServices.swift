//
//  UserServices.swift
//  BildelBasenClean
//
//  Created by Apple on 10/16/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation

class UserServices {
         // MARK: - UserLogin
    func loginUser(parameters: [String: Any], completion: @escaping (LoginModel?, Error?) -> ()) {
        // api
        let api = LoginAPI()
        // api loader
        let apiTaskLoader = APILoader(apiRequest: api)

        apiTaskLoader.loadAPIRequest(requestData: parameters) { (result) in
            switch result{
            case  .success(let loginModel):print("LoginSuccess")
                completion(loginModel,nil)
            case .failure(let error):
                 completion(nil,error)
            }
        }
    }
}
