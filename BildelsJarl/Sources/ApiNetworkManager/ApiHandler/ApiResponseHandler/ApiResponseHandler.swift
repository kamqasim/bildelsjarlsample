//
//  ApiResponseHandler.swift
//  BildelBasenClean
//
//  Created by Apple on 10/16/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation

protocol ResponseHandler {

        associatedtype ResponseDataType:Decodable

     func parseResponse(data: Data) throws -> ResponseDataType?
}

// MARK: - Response
protocol Response: Codable {
    var code: Int{get set}
    var message: String?{get set}
}

