//
//  AllergiesApiHandler.swift
//  BildelsJarl
//
//  Created by Apple on 10/30/19.
//  Copyright (c) 2019 Apple. All rights reserved.
//

import Foundation

struct AllergiesApiHandler: APIHandler {

    func makeRequest(from parameters: [String: Any]) -> Request {
        // prepare url
        let url = URL(string: Path.Allergy().getAllAlergies)
        var urlRequest = URLRequest(url: url!)
        // set http method type
        urlRequest.httpMethod = "POST"
        // set body params
        set(parameters, urlRequest: &urlRequest)
        // prepares request (sets header params, any additional configurations)
        let request = Request(urlRequest: urlRequest, requestBuilder: AuthRequest())
        return request
    }
   
    func parseResponse(data: Data) throws -> AllergiesModelList? {
    
        return try defaultParseResponse(data: data)
    }
}
