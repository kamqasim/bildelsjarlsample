//
//  UserApiHandelers.swift
//  BildelBasenClean
//
//  Created by Apple on 10/16/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation


struct LoginAPI: APIHandler {

    func makeRequest(from parameters: [String: Any]) -> Request {
        // prepare url
        let url = URL(string: Path.User().login)
        var urlRequest = URLRequest(url: url!)
        // set http method type
        urlRequest.httpMethod = "POST"
        // set body params
        set(parameters, urlRequest: &urlRequest)
        // prepares request (sets header params, any additional configurations)
        let request = Request(urlRequest: urlRequest, requestBuilder: DefaultRequest())
        return request
    }

    func parseResponse(data: Data) throws -> LoginModel? {
        return try defaultParseResponse(data: data)
    }
}

