//
//  AllergiesModel.swift
//  BildelsJarl
//
//  Created by Apple on 10/30/19.
//  Copyright (c) 2019 Apple. All rights reserved.
//

import Foundation



class AllergiesModelList : Response{

    var code: Int
    var message: String?
    var result:AllergiesResult?
}

struct AllergiesResult : Codable{

    var Alergies : [Alergies]
    var userAlergies : [Alergies]
}


struct Alergies : Codable{

    var allergyId                                   : Int
    var AllergyName                                 : String
    var selected                                    : Bool
    var price                                       : Int?

    init(allergyId : Int? , allergyName : String? , selected: Bool?) {
        self.allergyId = allergyId ?? 0
        self.AllergyName = allergyName ?? ""
        self.selected = selected ?? false
    }

    mutating func checkID(allergyId:Int) ->Bool{
        if allergyId == 0{
            self.price = 100
            return true
        }else{
            self.price = 200
             return false
        }
    }
}

class Excersise {

    var miles = 0
    var type: ExcersiseType
    var excersiseMode: ExcersiseMode
    init(type:ExcersiseType, excersiseMode:ExcersiseMode){
        self.type = type
        self.excersiseMode = excersiseMode
    }

    func start(minutes: Int) {
        var speed = 0
        if self.type == .Economy && self.excersiseMode == .Drive {
            speed = 35
        }
        if self.type == .OffRoad && self.excersiseMode == .Drive {
            speed = 50
        }
        if self.type == .Sport && self.excersiseMode == .Drive {
            speed = 70
        }
        self.miles = speed * (minutes / 60)
    }
}

enum ExcersiseType {
    case Economy
    case OffRoad
    case Sport
}

enum ExcersiseMode {
    case Park
    case Reverse
    case Neutral
    case Drive
}
